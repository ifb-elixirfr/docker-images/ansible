FROM docker.io/python:3.8-bullseye

ENV ANSIBLE_VERSION='4.8.0'

RUN pip install ansible==${ANSIBLE_VERSION} ansible-lint
